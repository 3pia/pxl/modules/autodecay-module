//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//      http://vispa.physik.rwth-aachen.de/ -
// Copyright (C) 2009-2012 Martin Erdmann   -
//               RWTH Aachen, Germany       -
// Licensed under a LGPL-2 or later license -
//-------------------------------------------

#include "pxl/core/logging.hh"
#include "pxl/algorithms/AutoDecayReconstruction.hh"
#include "pxl/algorithms/NeutrinoPz.hh"

#include <string>
#include <vector>
#include <limits>

namespace pxl
{

static Logger logger("pxl::AutoDecayReconstruction");

//-----------------------------------------------------------

TemplateParticle::TemplateParticle(const pxl::Particle* particle,
		const std::string& accept) :
	currentCandidate(0), metLepton(0), filter(0)
{
	this->particle = particle;
	explode(accept, candidateNames, true, ",");
	std::string str;
	if (particle->getUserRecords().get<std::string>(".AutoDecayReconstruction/compare", str))
		explode(str, compareCandidateNames, true, ",");

	if (particle->getUserRecords().get<double>(".AutoDecayReconstruction/mass_constraint",
			metMassConstraint))
		met = true;
	else
		met = false;

	if (particle->getUserRecords().get<std::string>(".AutoDecayReconstruction/filter", str))
	{
		filter = new ParticleScriptFilter();
		filter->create(str, "AutoDecayReconstruction Particle Filter");
		filter->beginJob("");
	}
	else
	{
		filter = 0;
	}
}

//-----------------------------------------------------------

TemplateParticle::~TemplateParticle()
{
	delete filter;
}

//-----------------------------------------------------------

void TemplateParticle::resetCandidates()
{
	currentCandidateIndex = 0;
	currentCandidate = 0;
	candidates.clear();
}

void TemplateParticle::resetCompareCandidates()
{
	currentCompareCandidateIndex = 0;
	currentCompareCandidate = 0;
	compareCandidates.clear();
}

void TemplateParticle::resetComparePermutation()
{
	currentCompareCandidateIndex = 0;
	currentCompareCandidate = compareCandidates[0];
}

//-----------------------------------------------------------

void TemplateParticle::checkCandidate(pxl::Particle *recparticle)
{
	// look for each accepted name in the eventview
	for (std::vector<std::string>::const_iterator ani = candidateNames.begin(); ani
			!= candidateNames.end(); ani++)
	{
		const std::string& name = *ani;

		// store the particle as candidate
		if (recparticle->getName() == name)
		{
			logger(LOG_LEVEL_DEBUG, "matching:", particle->getName(), " -> ",
					recparticle->getName());
			double result = 1.0;

			if (filter)
				result = filter->evaluate((pxl::Particle *) recparticle);

			if (result > 0.5)
			{
				candidates.push_back(recparticle);

				if (candidates.size() == 1)
				{
					currentCandidate = recparticle;
					currentCandidateIndex = 0;
				}

				//std::cout << " (ok)" << std::endl;
			}
			else
			{
				//std::cout << " (filtered)" << std::endl;
			}
		}
	}
}

const pxl::Particle* TemplateParticle::getCurrentCandidate()
{
	return currentCandidate;
}

void TemplateParticle::updateCurrentCandidate()
{
	currentCandidateIndex = 0;

	if (candidates.size() > 0)
		currentCandidate = candidates[0];
	else
		currentCandidate = 0;
}

bool TemplateParticle::nextCandidate()
{
	currentCandidateIndex++;

	if (currentCandidateIndex >= candidates.size())
	{
		// update the candidate pointer
		currentCandidateIndex = 0;
		currentCandidate = candidates[0];
		return true;
	}
	else
	{
		// update the candidate pointer
		currentCandidate = candidates[currentCandidateIndex];
		return false;
	}
}

const std::vector<const pxl::Particle*>& TemplateParticle::getCandidates() const
{
	return candidates;
}

std::vector<const pxl::Particle*>& TemplateParticle::getCandidates()
{
	return candidates;
}

//-----------------------------------------------------------

void TemplateParticle::checkCompareCandidate(const pxl::Particle *genparticle)
{
	logger(LOG_LEVEL_DEBUG, "comparing:", particle->getName(), " -> ",
			genparticle->getName());

	if (checkCompareCandidateParents(genparticle) == false)
		return;

	// look for each accepted name in the eventview
	for (std::vector<std::string>::const_iterator cni =
			compareCandidateNames.begin(); cni != compareCandidateNames.end(); cni++)
	{
		const std::string& name = *cni;

		// store the particle as candidate
		if (genparticle->getName() == name)
		{
			compareCandidates.push_back(genparticle);

			if (compareCandidates.size() == 1)
				currentCompareCandidate = genparticle;
		}

	}
}

bool TemplateParticle::checkCompareCandidateParents(
		const pxl::Particle *genparticle)
{
	const pxl::Particle *tplParent = 0;
	const pxl::Particle *genParent = 0;

	if (particle->getMotherRelations().size() > 0)
		tplParent
				= dynamic_cast<pxl::Particle*> (*particle->getMotherRelations().getContainer().begin());
	if (genparticle->getMotherRelations().size() > 0)
		genParent
				= dynamic_cast<pxl::Particle*> (*genparticle->getMotherRelations().getContainer().begin());

	while (tplParent != 0 && genParent != 0)
	{
		//std::cout << "        compare parent " << tplParent->getName() << "," << genParent->getName() << std::endl;
		std::vector < std::string > compareNames;
		std::string compare;

		// check if there is a compare constraint
		if (tplParent->getUserRecords().get<std::string>(".AutoDecayReconstruction/compare",
				compare))
			explode(compare, compareNames, true, ",");
		else
			return true;

		if (compareNames.size() == 0)
			return true;

		bool valid = false;
		for (std::vector<std::string>::const_iterator cni =
				compareNames.begin(); cni != compareNames.end(); cni++)
		{
			const std::string& name = *cni;

			// store the particle as candidate
			if (genParent->getName() == name)
			{
				valid = true;
				break;
			}
		}

		if (valid == false)
		{
			//std::cout << "        failed compare parent " << tplParent->getName() << " (" << compare << "): " << genParent->getName() << std::endl;
			return false;
		}

		if ((tplParent->getMotherRelations().size() > 0)
				&& (genParent->getMotherRelations().size() > 0))
		{
			tplParent
					= dynamic_cast<pxl::Particle*> (*tplParent->getMotherRelations().getContainer().begin());
			genParent
					= dynamic_cast<pxl::Particle*> (*genParent->getMotherRelations().getContainer().begin());
		}
		else
		{
			tplParent = 0;
			genParent = 0;
		}
	}

	return true;
}

const pxl::Particle* TemplateParticle::getCurrentCompareCandidate()
{
	return currentCompareCandidate;
}

bool TemplateParticle::nextCompareCandidate()
{
	currentCompareCandidateIndex++;

	if (currentCompareCandidateIndex >= compareCandidates.size())
	{
		// update the candidate pointer
		currentCompareCandidateIndex = 0;
		currentCompareCandidate = compareCandidates[0];
		return true;
	}
	else
	{
		// update the candidate pointer
		currentCompareCandidate
				= compareCandidates[currentCompareCandidateIndex];
		return false;
	}
}

const std::vector<const pxl::Particle*>& TemplateParticle::getCompareCandidates() const
{
	return compareCandidates;
}
const pxl::Particle* TemplateParticle::getParticle() const
{
	return particle;
}

TemplateParticle* TemplateParticle::getMetLepton()
{
	return metLepton;
}

void TemplateParticle::setMetLepton(TemplateParticle* tpl)
{
	metLepton = tpl;
}

bool TemplateParticle::isMet() const
{
	return met;
}

double TemplateParticle::getMetMassConstraint() const
{
	return metMassConstraint;
}

//-----------------------------------------------------------

AutoDecayReconstruction::AutoDecayReconstruction()
{
	_EventViewFilter = 0;
}

//-----------------------------------------------------------

AutoDecayReconstruction::~AutoDecayReconstruction()
{
}

//-----------------------------------------------------------

#ifndef NO_PYTHON_FILTER

//-----------------------------------------------------------

void AutoDecayReconstruction::createParticleFilter(
		const pxl::Particle* particle)
{
	std::string script;
	logger(LOG_LEVEL_INFO, "Find filter in user record. ");
	bool found = particle->getUserRecord<std::string>(".AutoDecayReconstruction/filter", script);
    
	logger(LOG_LEVEL_INFO, "Found filter in user record? ", found);
	if (found)
	{
        logger(LOG_LEVEL_INFO, "Found filter in user record: ", script);
		ParticleScriptFilter* filter = new ParticleScriptFilter();
		filter->create(script, "AutoDecayReconstruction Particle Filter");
		filter->beginJob("");
		_ParticleFilterMap[particle] = filter;
		logger(LOG_LEVEL_INFO, "ParticleScriptFilter created for: ",
				particle->getName());
	}
}

//-----------------------------------------------------------

void AutoDecayReconstruction::createEventViewFilter(
		const pxl::EventView* eventview)
{
	std::string script;
	eventview->getUserRecord(".AutoDecayReconstruction/filter", script);
	if (!script.empty())
	{
		_EventViewFilter = new EventViewScriptFilter();
		_EventViewFilter->create(script, "AutoDecayReconstruction Event Filter");
		_EventViewFilter->beginJob("");
		logger(LOG_LEVEL_INFO, "EventViewScriptFilter created for: ",
				eventview->getName());
	}
}

//-----------------------------------------------------------

bool AutoDecayReconstruction::evaluateParticle(
		const pxl::Particle* steeringParticle,
		pxl::Particle* processedParticle, double& result)
{
	std::map<const pxl::Particle*, ParticleScriptFilter*>::iterator i =
			_ParticleFilterMap.find(steeringParticle);
	if (i != _ParticleFilterMap.end())
	{
		result = i->second->evaluate(processedParticle);
		return true;
	}
	else
		return false;
}

//-----------------------------------------------------------

bool AutoDecayReconstruction::evaluateEventView(
		const pxl::EventView* steeringEventView,
		pxl::EventView* processedEventView, double& result)
{
	if (_EventViewFilter)
	{
		result = _EventViewFilter->evaluate(processedEventView);
		return true;
	}
	else
		return false;
}

#endif

bool shareParent(const pxl::Relative* a, const pxl::Relative* b)
{
	for (pxl::Relations::const_iterator i = a->getMotherRelations().begin(); i
			!= a->getMotherRelations().end(); i++)
	{
		for (pxl::Relations::const_iterator j = b->getMotherRelations().begin(); j
				!= b->getMotherRelations().end(); j++)
		{
			if (*i == *j)
				return true;
		}
	}

	return false;
}

bool AutoDecayReconstruction::removeIndistinguishables()
{
	logger(LOG_LEVEL_INFO, "check indistinguishables");

	for (std::vector<TemplateParticle*>::iterator i =
			_TemplateParticles.begin(); i != _TemplateParticles.end(); i++)
	{
		TemplateParticle *ta = *i;

		logger(LOG_LEVEL_DEBUG, "  ", ta->getParticle()->getName());

		std::vector<TemplateParticle*>::iterator j = i;
		for (j++; j != _TemplateParticles.end(); j++)
		{
			TemplateParticle *tb = *j;

			logger(LOG_LEVEL_DEBUG, "      -> ", tb->getParticle()->getName());

			std::set<const pxl::Particle *> intersection;

			if (shareParent(ta->getParticle(), tb->getParticle()))
			{
				// find particles in both
				for (std::vector<const pxl::Particle*>::iterator k =
						ta->getCandidates().begin(); k
						!= ta->getCandidates().end(); k++)
				{
					for (std::vector<const pxl::Particle*>::iterator l =
							tb->getCandidates().begin(); l
							!= tb->getCandidates().end(); l++)
					{
						if ((*k) == (*l))
						{
							logger(LOG_LEVEL_DEBUG, "      -> remove ",
									(*k)->getName());
							intersection.insert((*k));
						}
					}

				}

				// remove overlap
				for (std::set<const pxl::Particle*>::iterator k =
						intersection.begin(); k != intersection.end(); k++)
				{
					if (ta->getCandidates().size() > 1)
						ta->getCandidates().erase(std::remove(
								ta->getCandidates().begin(),
								ta->getCandidates().end(), *k),
								ta->getCandidates().end());
					else
						tb->getCandidates().erase(std::remove(
								tb->getCandidates().begin(),
								tb->getCandidates().end(), *k),
								tb->getCandidates().end());
				}

			}
		}

		for (std::vector<const pxl::Particle*>::iterator k =
				ta->getCandidates().begin(); k != ta->getCandidates().end(); k++)
		{
			logger(LOG_LEVEL_DEBUG, "    --", (*k)->id());
		}
	}

	for (std::vector<TemplateParticle*>::iterator i =
			_TemplateParticles.begin(); i != _TemplateParticles.end(); i++)
	{
		TemplateParticle *ta = *i;

		logger(LOG_LEVEL_DEBUG, "      ", ta->getParticle()->getName());
		for (std::vector<const pxl::Particle*>::iterator k =
				ta->getCandidates().begin(); k != ta->getCandidates().end(); k++)
		{
			logger(LOG_LEVEL_DEBUG, "      - ", (*k)->getName(), " ",
					(*k)->id());
		}

		ta->updateCurrentCandidate();
	}

	return true;
}

//-----------------------------------------------------------

void AutoDecayReconstruction::initialize(const pxl::EventView* steering)
{
	// make a full copy of the steering eventview
	_SteeringEventView = new pxl::EventView(*steering);

	// print the steering event view
	logger(LOG_LEVEL_INFO, "begin job ", _SteeringEventView->getName());

#ifndef NO_PYTHON_FILTER
	// setup scriptfilters
	createEventViewFilter(steering);
#endif
    
    logger(LOG_LEVEL_INFO, "Particle loop");
    
	// loop  over all feynman particles
	for (pxl::ObjectOwner::const_iterator i =
			_SteeringEventView->getObjectOwner().getObjects().begin(); i
			!= _SteeringEventView->getObjectOwner().getObjects().end(); i++)
	{
		pxl::Particle *particle = dynamic_cast<pxl::Particle*> (*i);
		if (particle == 0)
			continue;
        logger(LOG_LEVEL_INFO, "Loop over particle", particle->getName());
		std::string accept;
		if (particle->hasUserRecord(".AutoDecayReconstruction/accept"))
		    accept = particle->getUserRecord(".AutoDecayReconstruction/accept").toString();
		logger(LOG_LEVEL_INFO, "Accept particle name", accept);
		if (!accept.empty())
		{
			TemplateParticle* tp = new TemplateParticle(particle, accept);
			_TemplateParticles.push_back(tp);
		}
		else
		{
			createParticleFilter(particle);
		}
	}
	
    logger(LOG_LEVEL_INFO, "Find MET and lepton");
    
	// find the mets lepton
	for (std::vector<TemplateParticle*>::iterator i =
			_TemplateParticles.begin(); i != _TemplateParticles.end(); i++)
	{
		TemplateParticle* met_tp = *i;

		if (met_tp->isMet())
		{
			//set the algorithm to estimate the neutrino p_z if Im(p_z)!= 0; default is: take only the real: p_z = Re(p_z)
			_algorithm_to_estimate_neutrino_pz = 0;
			std::string _algorithm_to_estimate_neutrino_pz_temp = "default";
			std::string algorithm_check;
			met_tp->getParticle()->getUserRecord(
					".AutoDecayReconstruction/algorithm_imaginary_pz",
					_algorithm_to_estimate_neutrino_pz_temp);

			if (_algorithm_to_estimate_neutrino_pz_temp == "scaleWmass")
			{
				_algorithm_to_estimate_neutrino_pz = 1;
				algorithm_check = "scaleWmass";
			}
			else if (_algorithm_to_estimate_neutrino_pz_temp == "scaleMET")
			{
				_algorithm_to_estimate_neutrino_pz = 2;
				algorithm_check = "scaleMET";
			}
			else
			{
				_algorithm_to_estimate_neutrino_pz = 0;
				algorithm_check = "default";
			}

			logger(LOG_LEVEL_INFO, "Using algorithm ", algorithm_check,
					" to solve neutrino p_z; default is: p_z = Re(p_z)");

			for (std::vector<TemplateParticle*>::iterator j =
					_TemplateParticles.begin(); j != _TemplateParticles.end(); j++)
			{
				TemplateParticle* lep_tp = *j;

				if (shareParent(met_tp->getParticle(), lep_tp->getParticle()))
				{
					met_tp->setMetLepton(lep_tp);
					break;
				}
			}
		}
	}

	_SuccessfulEventCount = 0;
	_TotalEventCount = 0;
	logger(LOG_LEVEL_INFO, "Successful initialization");
}

//-----------------------------------------------------------

void AutoDecayReconstruction::shutdown()
{
	logger(LOG_LEVEL_INFO, "successfully processed ", _SuccessfulEventCount,
			" Events.");

#ifndef NO_PYTHON_FILTER
	for (std::map<const pxl::Particle*, ParticleScriptFilter*>::iterator i =
			_ParticleFilterMap.begin(); i != _ParticleFilterMap.end(); i++)
	{
		(*i).second->endJob();
		(*i).second->destroy();
		delete (*i).second;
	}

	delete _EventViewFilter;
#endif

	for (std::vector<TemplateParticle*>::iterator tpi =
			_TemplateParticles.begin(); tpi != _TemplateParticles.end(); tpi++)
		delete (*tpi);
	_TemplateParticles.clear();

	// delete private copy of steering eventview and name map
	delete _SteeringEventView;
}

//-----------------------------------------------------------

bool AutoDecayReconstruction::isCandidateInUse(const pxl::Particle *candidate,
		size_t index)
{
	// check if current candidate is already in use
	for (size_t i = index; i < _TemplateParticles.size(); i++)
		if (_TemplateParticles[i]->getCurrentCandidate() == candidate)
			return true;
	return false;
}

//-----------------------------------------------------------

bool AutoDecayReconstruction::nextPermutation()
{
	for (size_t i = 0; i < _TemplateParticles.size(); i++)
	{
		bool last = ((i + 1) == _TemplateParticles.size());
		bool overflow = _TemplateParticles[i]->nextCandidate();

		// if in use inc if not yet at end
		//while (isCandidateInUse(_TemplateParticles[i]->getCurrentCandidate(), i+1) == true && overflow == false)
		//	overflow = _TemplateParticles[i]->nextCandidate();
		if (overflow && last)
			return false;
		else if (overflow == false)
			return true;
	}

	return true;
}

//-----------------------------------------------------------

bool AutoDecayReconstruction::nextComparePermutation()
{
	for (size_t i = 0; i < _TemplateParticles.size(); i++)
	{
		if (_TemplateParticles[i]->getCompareCandidates().size() == 0)
			continue;

		bool overflow = _TemplateParticles[i]->nextCompareCandidate();

		if (overflow == false)
		{
			break;
		}
		else if ((i + 1) == _TemplateParticles.size())
		{
			return false;
		}
	}

	return true;
}

//-----------------------------------------------------------

bool AutoDecayReconstruction::checkPermutation()
{
	std::vector<TemplateParticle*>::iterator i;
	std::vector<TemplateParticle*>::iterator j;

	for (i = _TemplateParticles.begin(); i != _TemplateParticles.end(); i++)
	{
		for (j = i, j++; j != _TemplateParticles.end(); j++)
		{
			if ((*i)->getCurrentCandidate() == (*j)->getCurrentCandidate())
			{
				return false;
			}
		}
	}

	return true;
}

//-----------------------------------------------------------

bool AutoDecayReconstruction::checkComparePermutation()
{
	std::vector<TemplateParticle*>::iterator i;
	std::vector<TemplateParticle*>::iterator j;

	for (i = _TemplateParticles.begin(); i != _TemplateParticles.end(); i++)
	{
		if ((*i)->getCompareCandidates().size() == 0)
			continue;

		for (j = i, j++; j != _TemplateParticles.end(); j++)
		{
			if ((*j)->getCompareCandidates().size() == 0)
				continue;

			if ((*i)->getCurrentCompareCandidate()
					== (*j)->getCurrentCompareCandidate())
				return false;
			/*
			 // if templates share direct parents, generators need so too
			 if (shareParent((*i)->getParticle(), (*j)->getParticle()) &&
			 !shareParent((*i)->getCurrentCompareCandidate(), (*j)->getCurrentCompareCandidate()) )
			 {
			 if (_LogLevel >= LOG_LEVEL_WARNING)
			 std::cout << "AUTOPROCESS:    no parent match" << std::endl;
			 return false;
			 }
			 */
		}
	}

	return true;
}

//-----------------------------------------------------------

void AutoDecayReconstruction::createHypotheses(bool compare)
{
	bool run = true;

	while (run)
	{
		if (checkPermutation())
			createHypothesis(compare);

		run = nextPermutation();
	}
}

//-----------------------------------------------------------

size_t AutoDecayReconstruction::calculateNeutrinoPz(
		const pxl::Particle* lepton, const pxl::Particle* nu, double wMass,
		double& pz1, double& pz2)
{
	const pxl::LorentzVector& lepton4v = lepton->getVector();
	const pxl::LorentzVector& nu4v = nu->getVector();

	double leptonPt2 = lepton4v.getPt() * lepton4v.getPt();
	double m_w = wMass;

	double var1 = (lepton4v.getX() * nu4v.getX()) + (lepton4v.getY()
			* nu4v.getY());
	double var2 = lepton4v.getE() * lepton4v.getE();
	double var3 = leptonPt2 * nu4v.getPt() * nu4v.getPt();

	double A = (m_w * m_w) / 2.0 + var1;
	double Delta = var2 * (A * A - var3);

	bool p_z_has_oneSolution = false;

	double imaginary_part_pz = 0.;

	if (Delta < 0.)
	{
		p_z_has_oneSolution = true;

		//algorithms to solve imaginary part of neutrino solution
		if (_algorithm_to_estimate_neutrino_pz == 0)
		{// "default"
			imaginary_part_pz
					= calculateNeutrinoPz_algorithm_set_Im_part_to_zero(
							lepton4v, nu4v, wMass);
		}
		else if (_algorithm_to_estimate_neutrino_pz == 1)
		{//scaleWmass
			imaginary_part_pz = calculateNeutrinoPz_algorithm_scaleWmass(
					lepton4v, nu4v, wMass);
		}
		else if (_algorithm_to_estimate_neutrino_pz == 2)
		{//scaleMET
			imaginary_part_pz = calculateNeutrinoPz_algorithm_scaleMET(lepton,
					nu4v, wMass, _corrected_met);
		}
	}

	if (p_z_has_oneSolution)
	{
		pz1 = imaginary_part_pz;
		return 1;
	}
	else
	{
		double sqrtDelta = std::sqrt(Delta);

		pz1 = (A * lepton4v.getZ() + sqrtDelta) / leptonPt2;
		pz2 = (A * lepton4v.getZ() - sqrtDelta) / leptonPt2;

		return 2;
	}
}

//-----------------------------------------------------------

void AutoDecayReconstruction::updateFourVectors(pxl::EventView* eventview)
{
	// find incoming particle and recursivly update fourvectors
	for (pxl::ObjectOwner::const_iterator i =
			eventview->getObjectOwner().getObjects().begin(); i
			!= eventview->getObjectOwner().getObjects().end(); i++)
	{
		if ((*i)->getMotherRelations().size() > 0)
			continue;

		pxl::Particle *particle = dynamic_cast<pxl::Particle*> (*i);
		if (particle == 0)
			continue;

		particle->setP4FromDaughtersRecursive();
	}
}

//-----------------------------------------------------------

void AutoDecayReconstruction::createHypothesis(bool compare)
{
	logger(LOG_LEVEL_DEBUG, "permutation:");
	for (size_t i = 0; i < _TemplateParticles.size(); i++)
	{
		logger(LOG_LEVEL_DEBUG, "  ",
				_TemplateParticles[i]->getParticle()->getName(), " -> ",
				_TemplateParticles[i]->getCurrentCandidate()->getName(), "/",
				_TemplateParticles[i]->getCurrentCandidate()->id());
	}

	size_t neutrinoSolutionCount = 1;
	double neutrinoSolutions[2] =
	{ 0.0, 0.0 };

	for (std::vector<TemplateParticle*>::iterator i =
			_TemplateParticles.begin(); i != _TemplateParticles.end(); i++)
	{
		TemplateParticle* tp = *i;
		if (tp->isMet())
		{
			// find lepton to met
			neutrinoSolutionCount = calculateNeutrinoPz(
					tp->getMetLepton()->getCurrentCandidate(),
					tp->getCurrentCandidate(), tp->getMetMassConstraint(),
					neutrinoSolutions[0], neutrinoSolutions[1]);
			break;
		}
	}

	logger(LOG_LEVEL_DEBUG, "found ", neutrinoSolutionCount,
			" neutrino solutions.");

	if (neutrinoSolutionCount == 0)
		std::cout << "AUTOPROCESS:    Warning! No neutrino solution!"
				<< std::endl;

	for (size_t iNeutrinoSolution = 0; iNeutrinoSolution
			< neutrinoSolutionCount; iNeutrinoSolution++)
	{
		// copy template
		pxl::EventView* hypothesis = new pxl::EventView(*_SteeringEventView);
		hypothesis->setName("AutoDecayReconstruction");

		// replace reconstructed particles (check MET)
		std::vector<TemplateParticle*>::iterator tpi;
		for (tpi = _TemplateParticles.begin(); tpi != _TemplateParticles.end(); tpi++)
		{
			pxl::Particle* copy = hypothesis->findCopyOf<pxl::Particle> (
					(*tpi)->getParticle());
			//*copy = *(*tpi)->getCurrentCandidate();
			copy->setP4((*tpi)->getCurrentCandidate()->getVector());
			copy->setCharge((*tpi)->getCurrentCandidate()->getCharge());
			copy->setPdgNumber((*tpi)->getCurrentCandidate()->getPdgNumber());
			copy->setName((*tpi)->getCurrentCandidate()->getName());
			copy->getUserRecords()
					= (*tpi)->getCurrentCandidate()->getUserRecords();

			//special case: handling of the neutrino
			if ((*tpi)->isMet())
			{
				pxl::LorentzVector v = copy->getVector();

				//apply corrections if needed:
				if (_algorithm_to_estimate_neutrino_pz == 2)
				{
					const pxl::Id
							& uuid_lepton =
									(*tpi)->getMetLepton()->getCurrentCandidate()->getId();

					std::map<pxl::Id, std::pair<double, double> >::iterator
							corrected_map_iterator;
					corrected_map_iterator = _corrected_met.find(uuid_lepton);

					if (corrected_map_iterator != _corrected_met.end())
					{
						std::pair<double, double> my_corrected_met =
								(*corrected_map_iterator).second;
						double my_met_correction_factor =
								my_corrected_met.first / v.getPx();

						logger(
								LOG_LEVEL_DEBUG,
								"correct MET_px and MET_py related to particle with uuid ",
								uuid_lepton.toString(), " by a factor of ",
								my_met_correction_factor);

						copy->setUserRecord(
								".AutoDecayReconstruction/correction_factor_applied_to_px_py",
								my_met_correction_factor);
						v.setPx(my_corrected_met.first);
						v.setPy(my_corrected_met.second);
					}
				}
				v.setPz(neutrinoSolutions[iNeutrinoSolution]);
				v.setE(std::sqrt(v.getPx() * v.getPx() + v.getPy() * v.getPy()
						+ v.getPz() * v.getPz()));

				copy->setP4(v);
			}
		}

		// recalculate fourvectors of all parents
		updateFourVectors(hypothesis);
		if (compare && _GeneratorEventView != 0)
			compareHypotheses(hypothesis);
		_Hypotheses.push_back(hypothesis);
	}
}

//-----------------------------------------------------------

const std::vector<pxl::EventView*>& AutoDecayReconstruction::getHypotheses() const
{
	return _Hypotheses;
}

//-----------------------------------------------------------

void AutoDecayReconstruction::releaseHypotheses()
{
	_Hypotheses.clear();
}

//-----------------------------------------------------------

bool deltaRcmp(pxl::EventView* a, pxl::EventView* b)
{
	double dA, dB;

	if (a->getUserRecord<double> (".AutoDecayReconstruction/deltaR", dA)
			== false)
		return false;

	if (b->getUserRecord<double> (".AutoDecayReconstruction/deltaR", dB)
			== false)
		return true;

	return dA < dB;
}

//-----------------------------------------------------------

bool AutoDecayReconstruction::createHypotheses(
		const pxl::EventView* reconstructed, const pxl::EventView* generated)
{
	if (_TemplateParticles.size() == 0)
		return false;

	_GeneratorEventView = generated;

	logger(LOG_LEVEL_INFO, "reconstructEvent: begin ", _TotalEventCount);

	_TotalEventCount++;
	bool doCompare = false;

	// check eventview for accepted Particles
	for (std::vector<TemplateParticle*>::iterator tpi =
			_TemplateParticles.begin(); tpi != _TemplateParticles.end(); tpi++)
	{
		TemplateParticle* templateparticle = *tpi;

		for (pxl::ObjectOwner::const_iterator i =
				reconstructed->getObjectOwner().getObjects().begin(); i
				!= reconstructed->getObjectOwner().getObjects().end(); i++)
		{
			pxl::Particle *particle = dynamic_cast<pxl::Particle*> (*i);
			if (particle == 0)
				continue;

			templateparticle->checkCandidate(particle);
		}

		if (templateparticle->getCandidates().size() == 0)
		{
			logger(LOG_LEVEL_INFO, "Warning! No candidates! skipping event.");
			return false;
		}

		if (_GeneratorEventView)
		{
			for (pxl::ObjectOwner::const_iterator j =
					generated->getObjectOwner().getObjects().begin(); j
					!= generated->getObjectOwner().getObjects().end(); j++)
			{
				pxl::Particle *particle = dynamic_cast<pxl::Particle*> (*j);
				if (particle == 0)
					continue;

				//if (particle->getDaughterRelations().size() == 0)
				templateparticle->checkCompareCandidate(particle);
			}

			if (templateparticle->getCompareCandidates().size() == 0)
			{
				logger(
						LOG_LEVEL_INFO,
						"Warning! No matching candidates! no compare possible for template particle ",
						templateparticle->getParticle()->getName());
			}
			else
				doCompare = true;
		}

	}

	//removeIndistinguishables();

	for (std::vector<TemplateParticle*>::iterator tpi =
			_TemplateParticles.begin(); tpi != _TemplateParticles.end(); tpi++)
	{
		TemplateParticle* templateparticle = *tpi;

		if (templateparticle->getCandidates().size() == 0)
		{
			logger(LOG_LEVEL_INFO, "Warning! No candidates! skipping event.");
			return false;
		}
	}

	createHypotheses(doCompare);

	logger(LOG_LEVEL_INFO, "created ", _Hypotheses.size(), " hypotheses.");

	if (_Hypotheses.size() == 0)
	{
		logger(LOG_LEVEL_INFO, "no hypotheses created. skipping event.");
		return false;
	}

	// sort and mark best matched
	if (_GeneratorEventView)
	{
		std::sort(_Hypotheses.begin(), _Hypotheses.end(), deltaRcmp);

		if (_Hypotheses.size() > 0)
			_Hypotheses[0]->setName(_Hypotheses[0]->getName() + " BestMatch");
	}

	_SuccessfulEventCount++;

	logger(LOG_LEVEL_INFO, "reconstructEvent: end ", _SuccessfulEventCount);

	return true;
}

//-----------------------------------------------------------

void AutoDecayReconstruction::compareHypothesis(pxl::EventView* hypothesis)
{
	const double dPi = 3.1415926535897932384626433832795;
	double sumDeltaR = 0;

	logger(LOG_LEVEL_DEBUG, "compare permutation:");
	for (size_t i = 0; i < _TemplateParticles.size(); i++)
	{
		if (_TemplateParticles[i]->getCompareCandidates().size() == 0)
			continue;

		logger(LOG_LEVEL_INFO, _TemplateParticles[i]->getParticle()->getName(),
				" -> ",
				_TemplateParticles[i]->getCurrentCompareCandidate()->getName(),
				"/", _TemplateParticles[i]->getCurrentCompareCandidate()->id());
	}

	// replace reconstructed particles (check MET)
	std::vector<TemplateParticle*>::iterator tpi;
	for (tpi = _TemplateParticles.begin(); tpi != _TemplateParticles.end(); tpi++)
	{
		if ((*tpi)->getCompareCandidates().size() == 0)
			continue;

		pxl::Particle *rec = hypothesis->findCopyOf<pxl::Particle> (
				(*tpi)->getParticle());
		const pxl::Particle *gen = (*tpi)->getCurrentCompareCandidate();

		double dDphi = gen->getVector().getPhi() - rec->getVector().getPhi();
		while (dDphi > dPi)
			dDphi -= 2 * dPi;
		while (dDphi < -dPi)
			dDphi += 2 * dPi;

		double dDeta = gen->getVector().getEta() - rec->getVector().getEta();
		double DeltaR = sqrt(dDeta * dDeta + dDphi * dDphi);

		rec->setUserRecord(".AutoDecayReconstruction/deltaR-" + gen->getName(),
				DeltaR);

		sumDeltaR += DeltaR;
	}

	logger(LOG_LEVEL_INFO, "DeltaR: ", sumDeltaR);

	double currentDeltaR = std::numeric_limits<double>::infinity();
	hypothesis->getUserRecord<double> (".AutoDecayReconstruction/deltaR",
			currentDeltaR);
	if (sumDeltaR < currentDeltaR)
		hypothesis->setUserRecord(".AutoDecayReconstruction/deltaR", sumDeltaR);
}

//-----------------------------------------------------------

void AutoDecayReconstruction::compareHypotheses(pxl::EventView* hypothesis)
{
	for (std::vector<TemplateParticle*>::iterator tpi =
			_TemplateParticles.begin(); tpi != _TemplateParticles.end(); tpi++)
	{
		if ((*tpi)->getCompareCandidates().size() != 0)
			(*tpi)->resetComparePermutation();
	}

	bool run = true;
	while (run)
	{
		if (checkComparePermutation())
			compareHypothesis(hypothesis);

		run = nextComparePermutation();
	}
}

//-----------------------------------------------------------

void AutoDecayReconstruction::deleteHypotheses()
{
	logger(LOG_LEVEL_INFO, "deleteHypotheses");

	for (std::vector<pxl::EventView*>::iterator i = _Hypotheses.begin(); i
			!= _Hypotheses.end(); i++)
		delete (*i);
	_Hypotheses.clear();

	for (std::vector<TemplateParticle*>::iterator tpi =
			_TemplateParticles.begin(); tpi != _TemplateParticles.end(); tpi++)
	{
		(*tpi)->resetCandidates();
		(*tpi)->resetCompareCandidates();
	}
}

//-----------------------------------------------------------

void AutoDecayReconstruction::printStats()
{
	int total = _SuccessfulEventCount + _FailedEventCount;
	logger(LOG_LEVEL_INFO, "successful: ", _SuccessfulEventCount, " failed: ",
			_FailedEventCount, " total: ", total);
}

} // namespace pxl
