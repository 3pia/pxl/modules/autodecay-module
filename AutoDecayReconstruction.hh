//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//      http://vispa.physik.rwth-aachen.de/ -
// Copyright (C) 2009-2012 Martin Erdmann   -
//               RWTH Aachen, Germany       -
// Licensed under a LGPL-2 or later license -
//-------------------------------------------

#include "pxl/hep.hh"

#ifndef NO_PYTHON_FILTER
#include "pxl/scripting.hh"
#endif

namespace pxl {

/// Holds a list of reconstructed final state particles which are are accepted and passed the provided filter
class TemplateParticle
{
public:

	TemplateParticle(const Particle*, const std::string&);
	~TemplateParticle();

	const Particle* getParticle() const;

	void resetCandidates();
	void resetCompareCandidates();
	void resetComparePermutation();

	void checkCandidate(Particle *recparticle);
	const Particle* getCurrentCandidate();
	void updateCurrentCandidate();
	const std::vector<const Particle*>& getCandidates() const;
	std::vector<const Particle*>& getCandidates();
	bool nextCandidate();

	void checkCompareCandidate(const Particle *genparticle);
	bool checkCompareCandidateParents (const Particle *genparticle);
	const Particle* getCurrentCompareCandidate();
	const std::vector<const Particle*>& getCompareCandidates() const;
	bool nextCompareCandidate();

	bool isMet() const;
	TemplateParticle* getMetLepton();
	void setMetLepton(TemplateParticle*);
	double getMetMassConstraint() const;


private:

	const Particle* particle;

	std::vector<std::string> candidateNames;
	std::vector<const Particle*> candidates;
	size_t currentCandidateIndex;
	const Particle* currentCandidate;

	std::vector<std::string> compareCandidateNames;
	std::vector<const Particle*> compareCandidates;
	size_t currentCompareCandidateIndex;
	const Particle* currentCompareCandidate;

	bool met;
	double metMassConstraint;
	TemplateParticle* metLepton;

	ParticleScriptFilter* filter;

};

/**
 Automatic reconstruction of decay trees from a list of final state particles.
*/

class AutoDecayReconstruction
{
public:

	AutoDecayReconstruction();
	~AutoDecayReconstruction();

	/// Prepares the steering event view and creates the list of TemplateParticles.
	void initialize(const EventView* steering);

	/// Clears all buffers and TemplateParticles lists.
	void shutdown();

	/// Creates all possible hypotheses and compares them with the generator event if provided.
	bool createHypotheses(const EventView* reconstructed, const EventView* gemerated = 0);

	/// Deletes all hypotheses.
	void deleteHypotheses();

	/// Returns the list of hypotheses
	const std::vector< EventView*>& getHypotheses() const;

	/// Clears the list of hypotheses so they are not deleted on endEvent.
	void releaseHypotheses();

private:

	size_t _SuccessfulEventCount;
	size_t _TotalEventCount;
	int _FailedEventCount;
	short _algorithm_to_estimate_neutrino_pz;

	std::map<pxl::Id, std::pair<double, double> > _corrected_met; 	//map: <lepton uuid, <met_px, met_py> >

	void printStats();

	std::vector< TemplateParticle*> _TemplateParticles;
	std::vector< EventView*> _Hypotheses;
	const EventView* _GeneratorEventView;
	const EventView* _SteeringEventView;

	bool removeIndistinguishables();

	///
	bool nextPermutation();
	bool checkPermutation();
	bool isCandidateInUse (const Particle *, size_t);

	bool nextComparePermutation();
	bool checkComparePermutation();

	void createHypotheses(bool compare);
	void createHypothesis(bool compare);

	void compareHypotheses(EventView* hypothesis);
	void compareHypothesis(EventView* hypothesis);

	size_t calculateNeutrinoPz(const pxl::Particle*, const pxl::Particle*, double, double&,double&);

	void updateFourVectors(pxl::EventView*);

#ifndef NO_PYTHON_FILTER
	std::map<const Particle*,ParticleScriptFilter*> _ParticleFilterMap;
	EventViewScriptFilter* _EventViewFilter;

	void createParticleFilter(const Particle* particle);
	void createEventViewFilter(const EventView* eventview);

	bool evaluateParticle(const Particle* steeringParticle, Particle* processedParticle,
			double& result);
	bool evaluateEventView(const EventView* steeringEventView,
			EventView* processedEventView, double& result);
#endif
};

} // namespace pxl
