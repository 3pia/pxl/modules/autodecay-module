//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//      http://vispa.physik.rwth-aachen.de/ -
// Copyright (C) 2009-2012 Martin Erdmann   -
//               RWTH Aachen, Germany       -
// Licensed under a LGPL-2 or later license -
//-------------------------------------------

#include "AutoDecayReconstructionModule.hh"

#include "pxl/core/logging.hh"
#include "pxl/hep.hh"

#include <memory>

namespace pxl
{

static Logger logger("pxl::AutoDecayReconstructionModule");

AutoDecayReconstructionModule::AutoDecayReconstructionModule() :
	Module(), _InputFile(0)
{

}

void AutoDecayReconstructionModule::initialize() 
{
	addSink("in", "All Events from the input file.");
	addSource("out", "All Events from the input file.");

	addOption("filename",
			"Filename of the pxlio-File to use as steering event",
			 std::string(), OptionDescription::USAGE_FILE_OPEN);
	addOption("generator", "Name of the generator eventview",
			 "Generator", OptionDescription::USAGE_TEXT);
	addOption("reconstructed", "name of the reconstructed eventview.",
			 "Reconstructed", OptionDescription::USAGE_TEXT);
	addOption("match", "calculate best match",
			true);
	Module::initialize();
}

AutoDecayReconstructionModule::~AutoDecayReconstructionModule()
{
}

const std::string& AutoDecayReconstructionModule::getStaticType()
{
	static const std::string name("AutoDecayReconstruction");
	return name;
}

const std::string& AutoDecayReconstructionModule::getType() const
{
	return getStaticType();
}

bool AutoDecayReconstructionModule::isRunnable() const
{
	return false;
}

void AutoDecayReconstructionModule::beginRun() 
{
	getOption("generator", _GeneratorName);
	getOption("reconstructed", _ReconstructedName);
	getOption("match", _Match);
}

bool AutoDecayReconstructionModule::analyse(Sink *sink) 
{
	Event *event = dynamic_cast<Event *> (getSink("in")->get());

	if (event)
	{
		logger(LOG_LEVEL_DEBUG, getName(), " analyse");
		LogDispatcher::instance().pushIndent(' ');
		EventView *genevent = 0;
		EventView *recevent = 0;

		std::vector<Relative*>::const_iterator i;
		for (i = event->getObjectOwner().getObjects().begin(); i
				!= event->getObjectOwner().getObjects().end(); i++)
		{
			EventView *eventview = dynamic_cast<EventView *> (*i);
			if (eventview == 0)
				continue;

			if ((*i)->getName() == _GeneratorName)
				genevent = eventview;
			else if ((*i)->getName() == _ReconstructedName)
				recevent = eventview;
		}

		bool success = _AutoDecayReconstruction.createHypotheses(recevent, _Match ? genevent : 0);
		if (success)
		{
			for (std::vector<EventView*>::const_iterator i =
					_AutoDecayReconstruction.getHypotheses().begin(); i
					!= _AutoDecayReconstruction.getHypotheses().end(); i++)
				event->insertObject(*i);
			_AutoDecayReconstruction.releaseHypotheses();
		}
		_AutoDecayReconstruction.deleteHypotheses();

		if (success)
		{
			getSource("out")->setTargets(event);
			getSource("out")->processTargets();
		}

		LogDispatcher::instance().popIndent();

		return true;
	}
	else
	{
		logger(LOG_LEVEL_DEBUG, getName(), " analyse: false");
		return false;
	}
}

void AutoDecayReconstructionModule::beginJob() 
{
	try
	{
		std::string filename;
		getOption("filename", filename);
		filename = getAnalysis()->findFile(filename);
		if (filename.empty())
		{
			logger(LOG_LEVEL_INFO, getName(), ": opened ", filename);
			return;
		}

		_InputFile = new InputFile(filename);
		if (_InputFile->good())
		{
			logger(LOG_LEVEL_INFO, getName(), ": opened ", filename);
		}
		else
		{
			logger(LOG_LEVEL_INFO, getName(), ": failed to opened ", filename);
			safe_delete(_InputFile);
			return;
		}

		_InputFile->nextFileSection();

		std::auto_ptr<Event> event(new Event);
		bool read = _InputFile->readEvent(event.get());
		if (read == false)
		{
			_InputFile->nextFileSection();
			read = _InputFile->readEvent(event.get());
		}

		if (read)
		{
			std::vector<EventView *> eventviews;
			event->getObjectsOfType(eventviews);

			if (eventviews.size() == 0)
				throw std::runtime_error(
						"AutoDecayReconstructionModule, no steering event view found in file.");

			_AutoDecayReconstruction.initialize(eventviews.at(0));
		}
	} catch (...)
	{
		throw;
	}

}

void AutoDecayReconstructionModule::endJob() 
{
	_AutoDecayReconstruction.shutdown();
	safe_delete(_InputFile);
}

} // namespace pxl

