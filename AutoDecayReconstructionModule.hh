//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//      http://vispa.physik.rwth-aachen.de/ -
// Copyright (C) 2009-2012 Martin Erdmann   -
//               RWTH Aachen, Germany       -
// Licensed under a LGPL-2 or later license -
//-------------------------------------------

#ifndef PXL_MODULES_AUTODECAYRECONSTRUCTION_MODULE_HH
#define PXL_MODULES_AUTODECAYRECONSTRUCTION_MODULE_HH

#include "pxl/modules/Module.hh"
#include "pxl/core.hh"
#include "pxl/hep.hh"

#include "pxl/core/PluginManager.hh"
#include "pxl/modules/ModuleFactory.hh"
#include "pxl/core/macros.hh"

#include "AutoDecayReconstruction.hh"

namespace pxl {

/**
 module for running the AutoDecayReconstruction algorithm
 */

class PXL_DLL_EXPORT AutoDecayReconstructionModule : public Module
{
	AutoDecayReconstruction _AutoDecayReconstruction;

	std::string _GeneratorName;
	std::string _ReconstructedName;
	bool _Match;

	InputFile *_InputFile;

public:
	AutoDecayReconstructionModule();
	virtual ~AutoDecayReconstructionModule();

	static const std::string& getStaticType();
	virtual const std::string& getType() const;
	bool isRunnable() const;

	void initialize () ;

	void beginRun() ;

	bool analyse (Sink *sink) ;

	void beginJob() ;
	void endJob() ;
};

PXL_MODULE_INIT(AutoDecayReconstructionModule)
PXL_PLUGIN_INIT

}

#endif // PXL_MODULES_AUTOPROCESS_MODULE_HH

