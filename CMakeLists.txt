CMAKE_MINIMUM_REQUIRED (VERSION 2.6)
PROJECT (pxl-module)

# Make sure FindPXL.cmake is found.
SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}")

# set the name of the plugin
SET(PXL_MODULE_NAME AutoDecayReconstructionModule)

# add the plugin the list of shared libraries to be build
ADD_LIBRARY(${PXL_MODULE_NAME} MODULE AutoDecayReconstructionModule.cc AutoDecayReconstructionModule.hh AutoDecayReconstruction.hh AutoDecayReconstruction.cc NeutrinoPz.hh NeutrinoPz.cc)

# find PXL
FIND_PACKAGE(PXL)

# make sure the pxl modules code is linked
ADD_PXL_PLUGIN(pxl-core)
ADD_PXL_PLUGIN(pxl-modules)
ADD_PXL_PLUGIN(pxl-hep)

LINK_DIRECTORIES(${PXL_LIBRARY_DIRS})
INCLUDE_DIRECTORIES(${PXL_INCLUDE_DIRS})

# add the pxl libraries as dependencies
TARGET_LINK_LIBRARIES (${PXL_MODULE_NAME} ${PXL_LIBRARIES})

# Install the module in the user home directory
INSTALL(TARGETS ${PXL_MODULE_NAME} LIBRARY DESTINATION ${PXL_PLUGIN_INSTALL_PATH})