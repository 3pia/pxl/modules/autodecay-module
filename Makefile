all: AutoDecayReconstructionModule.so

clean:
	rm -rf AutoDecayReconstructionModule.so

install: AutoDecayReconstructionModule.so
	mkdir -p $(HOME)/.pxl-3.0/plugins
	cp AutoDecayReconstructionModule.so $(HOME)/.pxl-3.3/plugins

uninstall:
	rm -rf $(HOME)/.pxl-3.0/plugins/AutoDecayReconstructionModule.so
	 
AutoDecayReconstructionModule.so: AutoDecayReconstructionModule.cc AutoDecayReconstructionModule.hh AutoDecayReconstruction.hh AutoDecayReconstruction.cc NeutrinoPz.hh NeutrinoPz.cc
	g++ -shared -fPIC -o AutoDecayReconstructionModule.so `pkg-config 'pxl-core >= 3.0.0 pxl-modules >= 3.0.0 pxl-hep >= 3.0.0' --libs --cflags` `root-config --libs --cflags` AutoDecayReconstructionModule.cc AutoDecayReconstruction.cc NeutrinoPz.cc

	