//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//      http://vispa.physik.rwth-aachen.de/ -
// Copyright (C) 2009-2012 Martin Erdmann   -
//               RWTH Aachen, Germany       -
// Licensed under a LGPL-2 or later license -
//-------------------------------------------

#include "NeutrinoPz.hh"
#include "pxl/core/logging.hh"

#include <string>

namespace pxl {

static Logger logger("pxl::NeutrinoPz");

/*size_t NeutrinoPz::calculateNeutrinoPz(const pxl::Particle* lepton,const pxl::Particle* nu, double wMass, double& pz1, double& pz2)
{
	const pxl::LorentzVector& lepton4v = lepton->getVector();
	const pxl::LorentzVector& nu4v = nu->getVector();

	double leptonPt2 = lepton4v.getPt()*lepton4v.getPt();
	double m_w = wMass;

	double var1 = (lepton4v.getX() * nu4v.getX()) + (lepton4v.getY() * nu4v.getY());
	double var2 = lepton4v.getE() * lepton4v.getE();
	double var3 = leptonPt2	* nu4v.getPt() * nu4v.getPt();

	double A = (m_w*m_w) / 2.0 + var1;
	double Delta = var2 * (A*A - var3);

	bool p_z_has_oneSolution = false;

	double imaginary_part_pz = 0.;

	if (Delta < 0.)
	{
		p_z_has_oneSolution = true;

		//algorithms to solve imaginary part of neutrino solution
		if (_algorithm_to_estimate_neutrino_pz == 0)
		{// "default"
			imaginary_part_pz = calculateNeutrinoPz_algorithm_set_Im_part_to_zero(lepton4v,nu4v,wMass);
		}
		else if (_algorithm_to_estimate_neutrino_pz == 1)
		{//scaleWmass
			imaginary_part_pz = calculateNeutrinoPz_algorithm_scaleWmass(lepton4v,nu4v,wMass);
		}
		else if (_algorithm_to_estimate_neutrino_pz == 2)
		{//scaleMET
			imaginary_part_pz = calculateNeutrinoPz_algorithm_scaleMET(lepton,nu4v,wMass);
		}
	}

	if (p_z_has_oneSolution)
	{
		pz1 = imaginary_part_pz;
		return 1;
	}
	else
	{
		double sqrtDelta = std::sqrt(Delta);

		pz1 = (A * lepton4v.getZ() + sqrtDelta) / leptonPt2;
		pz2 = (A * lepton4v.getZ() - sqrtDelta) / leptonPt2;

		return 2;
	}
}*/

//-----------------------------------------------------------

double calculateNeutrinoPz_algorithm_scaleMET(const pxl::Particle* lepton, const pxl::LorentzVector& nu4v, double wMass, std::map<pxl::Id, std::pair<double, double> >& _corrected_met)
{
	const pxl::LorentzVector& lepton4v = lepton->getVector();

	double leptonPt2 = lepton4v.getPt()*lepton4v.getPt();
	double m_w = wMass;

	double met_px_input = nu4v.getX();
	double met_py_input = nu4v.getY();

	double met_px_scaled = nu4v.getX();
	double met_py_scaled = nu4v.getY();

	double var1 = (lepton4v.getX() * met_px_input) + (lepton4v.getY() * met_py_input);
	double var2 = lepton4v.getE() * lepton4v.getE();
	double var3 = leptonPt2	* (met_px_input * met_px_input + met_py_input * met_py_input);

	double A = (m_w*m_w) / 2.0 + var1;
	double Delta = var2 * (A*A - var3);

	short count_iterations = -1;
	short max_iterations = 100;

	float step = 0.1;
	float factor = 1.;

	//be very precise to steps as small changes to met_px and met_py have much influence on delta, but very few influences on met_px and met_py
	while (step >= 0.000000001)
	{
		count_iterations += 1;

		//met will be always decreased, this follows from the p_z - equation
		factor -=  step;

		// increase met_px and met_py
		met_px_scaled *= factor;
		met_py_scaled *= factor;

		//recalculate terms
		var1 = (lepton4v.getX() * met_px_scaled) + (lepton4v.getY() * met_py_scaled);
		var3 = leptonPt2 * (met_px_scaled*met_px_scaled+met_py_scaled*met_py_scaled);
		A = (m_w*m_w) / 2.0 + var1;
		Delta = var2 * (A*A - var3);

		logger(LOG_LEVEL_DEBUG, "scaleMET: #" , count_iterations, ", fix delta: ", Delta, ", factor: ", factor);
		logger(LOG_LEVEL_DEBUG, "  met_px: ", nu4v.getX(), " -> ", met_px_scaled);
		logger(LOG_LEVEL_DEBUG, "  met_py: ", nu4v.getY(), " -> ", met_py_scaled);

		if (Delta >= 0.0)
		{
				factor +=  step;
				step /= 2.0;
		}

		met_px_scaled = met_px_input;
		met_py_scaled = met_py_input;

		if (count_iterations > max_iterations)
		{
			break;
		}
	}

/*
	if (fabs(Delta) >= 100.0)
	{
		logger(LOG_LEVEL_DEBUG, "scaleMET: ", count_iterations, " Approximation by this algorithm is not really good; one has to increase max_iterations or to choose another algorithm!");
	}
	*/
	met_px_scaled = met_px_input * factor;
	met_py_scaled =  met_py_input* factor;

	//MET fourvector should be corrected, but one cannot correct the templateParticle directly as this MET TP is input for other lepton(1,2,3,..) <->neutrino hypotheses
	//map lepton ID and correction factor
	_corrected_met.insert(std::pair<pxl::Id,  std::pair<double, double> > (lepton->getId(), std::pair<double, double>(met_px_scaled, met_py_scaled)));

	logger(LOG_LEVEL_DEBUG, "scaleMET: #" , count_iterations, ", fix delta: ", Delta, ", factor: ", factor);
	logger(LOG_LEVEL_DEBUG, "  met_px: ", nu4v.getX(), " -> ", met_px_scaled);
	logger(LOG_LEVEL_DEBUG, "  met_py: ", nu4v.getY(), " -> ", met_py_scaled);

	return ((A * lepton4v.getZ()) / leptonPt2);
}

//-----------------------------------------------------------

double calculateNeutrinoPz_algorithm_set_Im_part_to_zero(const pxl::LorentzVector& lepton4v, const pxl::LorentzVector& nu4v, double wMass)
{
	double leptonPt2 = lepton4v.getPt()*lepton4v.getPt();

	double var1 = (lepton4v.getX() * nu4v.getX()) + (lepton4v.getY() * nu4v.getY());
	double A = (wMass*wMass) / 2.0 + var1;

	return ((A * lepton4v.getZ()) / leptonPt2);
}

//-----------------------------------------------------------

double calculateNeutrinoPz_algorithm_scaleWmass(const pxl::LorentzVector& lepton4v, const pxl::LorentzVector& nu4v, double wMass)
{
	double leptonPt2 = lepton4v.getPt()*lepton4v.getPt();
	double m_w = wMass;

	double var1 = (lepton4v.getX() * nu4v.getX()) + (lepton4v.getY() * nu4v.getY());
	double var2 = lepton4v.getE() * lepton4v.getE();
	double var3 = leptonPt2	* nu4v.getPt() * nu4v.getPt();

	double A = (m_w*m_w) / 2.0 + var1;
	double Delta = var2 * (A*A - var3);

	double step = 10.0;
	int iteration  = 0;

	while (step >= 0.0001)
	{
		iteration += 1;

		m_w += step;

		A = (m_w*m_w) / 2.0 + var1;
		Delta = var2 * (A*A - var3);

		logger(LOG_LEVEL_DEBUG, "scaleWmass: #", iteration, ", fix delta: ", Delta);
		logger(LOG_LEVEL_DEBUG, "  m_W: ", m_w, ", step:", step);

		if (Delta >= 0.0)
		{
			m_w -= step;
			step /= 2.0;
		}
	}

//		/*if (fabs(Delta) >= 100.0)
//		{
//			std::cout << "(scaleWmass): Approximation by this algorithm is not really good; one has to increase max_iterations or to choose another algorithm!" << std::endl;
//		}*/
//	}
	logger(LOG_LEVEL_DEBUG, "scaleWmass: fixed delta: ",  Delta, ", m_W: ", m_w);

	double result = ((A * lepton4v.getZ()) / leptonPt2);

	return result;
}


} // namespace pxl
