//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//      http://vispa.physik.rwth-aachen.de/ -
// Copyright (C) 2009-2012 Martin Erdmann   -
//               RWTH Aachen, Germany       -
// Licensed under a LGPL-2 or later license -
//-------------------------------------------

#ifndef PXL_ALGORITHMS_NEUTRINOPZ_HH
#define PXL_ALGORITHMS_NEUTRINOPZ_HH

#include "pxl/core/macros.hh"
#include "pxl/hep.hh"

namespace pxl {

		double calculateNeutrinoPz_algorithm_scaleMET(const pxl::Particle*,const pxl::LorentzVector&, double, std::map<pxl::Id, std::pair<double, double> >&);
		double calculateNeutrinoPz_algorithm_set_Im_part_to_zero(const pxl::LorentzVector&, const pxl::LorentzVector&, double);
		double calculateNeutrinoPz_algorithm_scaleWmass(const pxl::LorentzVector&, const pxl::LorentzVector&, double);

} // namespace pxl

#endif // PXL_ALGORITHMS_NEUTRINOPZ_HH
